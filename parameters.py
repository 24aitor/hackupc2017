import json
filename = "parameters.json"

def updateParameter(key, value):
    with open(filename, 'r') as f:
        data = json.load(f)
        data[key] = value

    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)

def readParameter(key):
    with open(filename, 'r') as f:
        data = json.load(f)
        return data[key]
