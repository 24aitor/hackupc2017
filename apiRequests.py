import requests
from parameters import *
from rooms import *
from NicePrinter import *

class Server(object):
    """docstring for Server."""
    def __init__(self, api_token = readParameter('api_token'), headers = {'X-Requested-With': 'XMLHttpRequest'}, base_url = "https://homenet.erik.cat/api/house/"):
        """
        Initializates the server
        """
        self.api_token = api_token
        self.headers = headers
        self.base_url = base_url


    def doRequest(self, endpoint, data = {}, sendApiToken = True):
        """
        Perform a requests and returns the json response
        """
        if (sendApiToken == True):
            data['api_token'] = self.api_token
        response = requests.post(self.base_url + endpoint, data = data, headers = self.headers)
        content = response.json()
        return content



def newHouse():
    """
    Create a new house to the database
    """
    ser = Server()
    content = ser.doRequest('new_house', {}, False)
    updateParameter("api_token", content["api_token"])
    return content["auth_token"]


def newRoom(switch_id, switch_api_send_key, switch_api_receive_key, sensor_id, sensor_api_key):
    ser = Server()
    content = ser.doRequest('new_room')
    print (content)
    room_id = content['id']
    with open('devices.json', 'r') as f:
        data = json.load(f)
        switch = {"device_id" : switch_id, "type" : "switch", "api_send_key" : switch_api_send_key, "api_receive_key" : switch_api_receive_key, "room_id" : room_id, "last_value" : 0}
        sensor = {"device_id" : sensor_id, "type" : "sensor", "api_key" : sensor_api_key, "room_id" : room_id}
        data.append(switch)
        data.append(sensor)

    with open('devices.json', 'w') as f:
        json.dump(data, f, indent=4)


def readRoom(room_id):
    """
    Reads a room from the database passing a valid id
    """
    ser = Server()
    return ser.doRequest('room', {"id" : room_id})


def readRooms():
    """
    Reads all rooms from the database of this house
    """
    ser = Server()
    content = ser.doRequest('rooms')
    return content


def setRoomTemp(room_id, temp):
    """
    You should pass temps as float (5,2)
    """
    ser = Server()
    content = ser.doRequest('set_room_temp', {"id" : room_id, "temp" : temp})
    return content

def setRoomPresence(room_id, presence):
    """
    As presence you can pass 0 or 1 as integer
    """
    ser = Server()
    content = ser.doRequest('set_room_presence', {"id" : room_id, "presence" : presence})
    return content


def setRoomLight(room_id, light):
    """
    As light you can pass 0 or 1 as integer
    """
    ser = Server()
    content = ser.doRequest('set_room_light', {"id" : room_id, "light" : light})
    return content

def getHomeInfo():
    """
    As light you can pass 0 or 1 as integer
    """
    ser = Server()
    content = ser.doRequest('info', {})
    return content

    room_id = content['id']
    with open('devices.json', 'r') as f:
        data = json.load(f)
        switch = {"device_id" : switch_id, "type" : "switch", "api_send_key" : switch_api_send_key, "api_receive_key" : switch_api_receive_key, "room_id" : room_id, "last_value" : 0}
        sensor = {"device_id" : sensor_id, "type" : "sensor", "api_key" : sensor_api_key, "room_id" : room_id}
        data.append(switch)
        data.append(sensor)

if __name__ == '__main__':

    # print (readParameter('api_token'))

    # auth_token = newHouse()
    # print (cbbox(purple("Auth token: ") + yellow(auth_token)))
    #
    # print (hr())
    #
    print (hr())
    #
    a = readRoom(5)
    print (a)
    #
    #
    # newRoom("abc", "def", "ghj", "klw", "qwe")

    # setRoomTemp(33, 59.99)
