from flask import Flask, request, jsonify
from parameters import *
from apiRequests import *

room = {"switch" : 0, "sensor" : 0}

app = Flask(__name__)


@app.route('/presence', methods=['POST', 'GET'])
def presence():
    content = request.json
    api_key = readParameter("api_key")
    if (api_key == content["api_key"]):
        return jsonify({"code" : 200, "message" : "Ok"})
        return content["api_key"] + "--" + content["device_id"] + "--" + str(content["data"])

    else:
        return jsonify({"code" : 403, "message" : "Unauthorized"})


@app.route('/new_connection', methods=['POST'])
def new_connection():
    content = request.json
    api_key = readParameter("api_key")
    if (api_key == content["api_key"]):
        # dic['switch_id'] = 3
        if (content['type'] == "switch"):
            if (room["switch"] == 0):
                room["switch"] = content['device_id']
            else:
                return jsonify({"code" : 500, "message" : "Waiting for a sensor device, not a switch"})

        elif (content['type'] == "sensor"):
            if (room["sensor"] == 0):
                room["sensor"] = content['device_id']
            else:
                return jsonify({"code" : 500, "message" : "Waiting for a switch device, not a sensor"})

        if ((room["switch"] != 0) and (room["sensor"] != 0)):
            newRoom(room["switch"], room["sensor"], readParameter('api_token'))

            room["switch"] = 0
            room["sensor"] = 0


        # return content["api_key"] + "--" + content["device_id"] + "--" + str(content["data"])
        return jsonify({"code" : 200, "message" : "Ok"})

    else:
        return jsonify({"code" : 403, "message" : "Unauthorized"})



@app.route("/")
def hello():
    return "Hello World!"


@app.route("/ides")
def ides():
    return jsonify(room)
