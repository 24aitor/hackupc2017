from flask import Flask, request, jsonify
from parameters import *
from apiRequests import *
import datetime
import uuid
# import gpio

rooms = readRooms()

room = {"switch_id" : 0, "switch_api_send_key" : 0, "switch_api_receive_key" : 0, "sensor_id" : 0, "sensor_api_receive_key" : 0}

app = Flask(__name__)

api_keys = {}

hysteresis = 0

lux_index = {}

dates = {}

@app.route('/presence', methods=['POST'])
def presence():
    content = request.json
    api_key = readParameter("api_key")
    if (authorize(content['id'], content['api_key'], "receive")):
        with open('devices.json', 'r') as f:
            data = json.load(f)
            for i in data:
                if (i["device_id"] == content["id"]):
                    room_id = i["room_id"]
        setRoomPresence(room_id, content["presence_in_room"])
        room_data = readRoom(room_id)
        if (content["presence_in_room"]):
            global lux_index
            if (room_data["light_threeshold"] > lux_index[room_id]):
                setLightOn[room_id] = 1
                dates[room_id] = datetime.datetime.now()
        else:
            print ("H")
            # if (presence_activates_light):
                # if (datetime.timedelta(dates[room_id]) > preset_timeout):

    else:
        return jsonify({"code" : 403, "message" : "Unauthorized"})

def authorize(device_id, api_key, direction):
    with open('devices.json', 'r') as f:
        data = json.load(f)
        for i in data:
            if (i["device_id"] == device_id):
                if(direction == "receive"):
                    apiname = "api_key"
                    if (i["type"] == "switch"):
                        apiname = "api_receive_key"
                else:
                    apiname = "api_send_key"
                if (i["api_key"] == api_key):
                    return True
                else:
                    return False
        return False

@app.route('/lightntemp', methods=['POST'])
def lightntemp():
    content = request.json
    if (authorize(content['id'], content['api_key'], "receive")):
        with open('devices.json', 'r') as f:
            data = json.load(f)
            for i in data:
                if (i["device_id"] == content['id']):
                    setRoomTemp(i["room_id"], content["temp"])
                    room_id = i["room_id"]

        data = getHomeInfo()
        room_data = readRoom(room_id)

        set_temp = data["set_temp"]

        if (data["room_id"] == None):
            temp = 0
            counter = 0
            for i in readRooms():
                temp += i["temp"]
                counter += 1
            temperature = float(temp / counter)
            house = getHomeInfo()
        else:
            temperature = room_data["temp"]

        global hysteresis
        if (hysteresis):
            if (set_temp > (temperature + float(data["hysteresis_threshold"]/2))):
                hysteresis = 0
                print ("ON")
                # gpio.set(7, 0)
                # gpio.end(7)
        else:
            if (set_temp < (temperature - float(data["hysteresis_threshold"]/2))):
                hysteresis = 1
                print ("OFF")
                # gpio.start(7)
                # gpio.set(7, 1)

        global lux_index
        lux_index[room_id] = content["lux_index"]
        return jsonify({"code" : 200, "message" : "Ok"})

    else:
        return jsonify({"code" : 403, "message" : "Unauthorized"})


@app.route('/new_credentials', methods=['POST'])
def new_credentials():
    content = request.json
    sensorType = content["type"]

    new_id = int(readParameter("last_device_id")) + 1
    updateParameter('last_device_id', new_id)

    if (sensorType == "switch"):
        api_send_key = uuid.uuid4()
        api_receive_key = uuid.uuid4()
        global api_keys
        api_keys = {"api_receive_key" : api_receive_key, "api_send_key" : api_send_key, "id" : new_id}
        return jsonify(api_keys)

    elif (sensorType == "sensor"):
        api_send_key = uuid.uuid4()
        global api_keys
        api_keys = {"api_key" : api_send_key, "id" : new_id}
        return jsonify(api_keys)

    return False

@app.route('/new_connection', methods=['POST'])
def new_connection():
    content = request.json
    if (content['type'] == "switch"):
        if ((api_keys['id'] == content['id']) and (str(api_keys["api_send_key"]) == content["api_send_key"]) and (str(api_keys["api_receive_key"]) == content["api_receive_key"])):
            if (room["switch_id"] == 0):
                room["switch_id"] = content['id']
                room["switch_api_send_key"] = content['api_send_key']
                room["switch_api_receive_key"] = content['api_receive_key']
            else:
                return jsonify({"code" : 500, "message" : "Waiting for a sensor device, not a switch"})

    elif (content['type'] == "sensor"):
        if ((api_keys['id'] == content['id']) and (str(api_keys["api_key"]) == content["api_key"])):
            if (room["sensor_id"] == 0):
                room["sensor_id"] = content['id']
                room["sensor_api_key"] = content['api_key']
            else:
                return jsonify({"code" : 500, "message" : "Waiting for a switch device, not a sensor"})

    else:
        return jsonify({"code" : 403, "message" : "Bad call to method, unrecognized type"})

    if ((room["switch_id"] != 0) and (room["sensor_id"] != 0)):
        newRoom(room["switch_id"], room["switch_api_send_key"], room["switch_api_receive_key"], room["sensor_id"], room["sensor_api_key"])

        room["switch_id"] = 0
        room["sensor_id"] = 0

    return jsonify({"code" : 200, "message" : "Ok"})


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/ides")
def ides():
    return jsonify(api_keys)

@app.route("/roomsfun")
def roomsfun():
    return jsonify(room)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port = 8080)
