import json

filename = "devices.json"

def saveRoom(switch_id, switch_api_send_key, switch_api_receive_key, sensor_id, sensor_api_key):
    with open(filename, 'r') as f:
        data = json.load(f)
        payload = {"switch_id" : switch_id, "switch_api_send_key" : switch_api_send_key, "switch_api_receive_key" : switch_api_receive_key, "sensor_id" : sensor_id, "sensor_api_key" : sensor_api_key}
        data.append(payload)

    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


def getRoomId(device_id):
    for room in rooms:
        if ((room["switch_id"] == device_id) or (room["sensor_id"] == device_id)):
            return room["room_id"]

if __name__ == '__main__':
    newRoom(1,2,3)
