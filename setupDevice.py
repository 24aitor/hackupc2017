from parameters import *

import uuid


def newDevice(sensorType):

    new_ip = int(readParameter("last_device_ip")) + 1
    updateParameter('last_device_ip', new_ip)

    if (sensorType == "switch"):
        api_send_key = uuid.uuid4()
        api_receive_key = uuid.uuid4()
        return api_receive_key, api_send_key, new_ip

    elif (sensorType == "sensor"):
        api_receive_key = uuid.uuid4()
        return api_receive_key, new_ip

    return False


if __name__ == '__main__':

    a,b = newDevice("sensor")
    print(a)
    print(b)
